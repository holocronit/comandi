  /* ================= REQUIRED LIBRARIES =============== */
/*
 * 
 * 
 */
#include <SerialCommand.h>
#include <SoftwareSerial.h>

SerialCommand SCmd;
#define DELAY_LETT_SENS             1000

/* ====================================================== */
/* ===================  CLASS DEFINE  =================== */
/* ====================================================== */

unsigned long int precLettSens = 0;
bool skipAll = true;

/* ====================================================== */
/* ================= END CLASS DEFINE  ================== */
/* ====================================================== */


/*
*
* MAIN SETUP FUNC
*
*/
void setup()
{    
                  
  // Inizializzo Seriale
  Serial.begin( 500000 );
  
  
  /* ===================  SERIAL COMANDS  =================== */
  // PETALI
  SCmd.addCommand("CPE",state);
  SCmd.addCommand("LSP",ReadStatusPetal);
  SCmd.addCommand("CPS",StopPetal);
  // MOTORI
  SCmd.addCommand("CMM",motor);
  SCmd.addCommand("LMP",ReadPos);
  SCmd.addCommand("CMI",infinito);
  SCmd.addCommand("CMS",stopp);
  SCmd.addCommand("LMO",ReadAllPos);
  SCmd.addCommand("LMS",ReadStatus);
  SCmd.addCommand("LMG",ReadAllMot);
  SCmd.addCommand("SET",setParameter);
  SCmd.addCommand("GET",getParameter);
  SCmd.addCommand("GOTO",goToPosition);
  //SENSORI
  SCmd.addCommand("LST",ReadTemp);
  SCmd.addCommand("LHU",ReadHumi);
  SCmd.addCommand("LFP",ReadFocPri);
  SCmd.addCommand("LFS",ReadFocSec);
  SCmd.addCommand("LTE",ReadAllSensors); 

  //I/O
  SCmd.addCommand("CFN",SetVentole);
  SCmd.addCommand("CPO",SetScaldini);
  SCmd.addCommand("LOU",GetOutput);
  SCmd.addCommand("SWC",SetSwitch);
  SCmd.addCommand("SWS",StatusSingleSwitch);
  SCmd.addCommand("SWA",StatusSwitch);

  //LCD
  SCmd.addCommand("CDS",PrintLcd);


  //DEBUG
  SCmd.addCommand("DEBUG",debug);
  SCmd.addCommand("TEST",test);
  SCmd.addCommand("ALL",readAll);
  SCmd.addCommand("ALU",manageAll);

  //DEFAULT
  SCmd.addDefaultHandler(unrecognized); 

}


/*
*
* MAIN LOOP FUNC
*
*/
void loop()
{
   // Handler comandi seriale
   SCmd.readSerial();

  if ( millis() - precLettSens >= DELAY_LETT_SENS  ) {
      readAllSyncro();
      precLettSens = millis();
  }
    


}




/* ================================================================== */
/* ======================== CAMANDS CALLBACKS  ====================== */
/* ================================================================== */


/* ===========================  PETAL  ============================= */
/*
 * 
 *
 *
  */
void state(){
  
  Serial.println("ok-CPE");
  
}
void ReadStatusPetal(){
  
  Serial.println("ok-LSP| 1|1 2|1 3|2 4|3");
  
}

void StopPetal(){
  
  Serial.println("ok-CPS");
  
}
/* ===========================  MOTORI  ============================= */
/*
 * 
 * 
 * 
 */
/* =============   MOVO MOTOR  ========== */

/*
 * 
 * 
  */
 
void motor() {

  Serial.println("ok-CMM");
  
}

/* =============   READ POSITION  ========== */

/*
 * 
 *
 *
  */
void ReadPos(){
  
    Serial.println("ok-LMP|100"); 

}

/* ===============   INFINITY MOVE  ============ */

/*
 * 
 *
  */
void infinito(){
   
    Serial.println("ok-CMI");
    
}

/* =================   STOP MOTOR  ============== */

/*
 * 
 *
 *
  */
void stopp(){    
  
    Serial.println("ok-CMS");
    
}

/* ==============   READ ALL POSITION  =========== */

/*
 * 
 *
 *
  */
void ReadAllPos(){
  
  Serial.println("ok-LMO|200|150|50");
  
}

/* ==============   READ STATUS MOTOR  ============= */

/*
 * 
 *
 *
  */
void ReadStatus(){
  
  Serial.println("ok-LMS|F|B|B");
  
}

/* ==============   READ ALL INFO MOTOR  =========== */

/*
 * 
 *
 *
  */
void ReadAllMot(){
  
  Serial.println("ok-LMG|F 100|B 200|F 120");
  
}

/* ======================   SET  =================== */

/*
 * 
 *
 *
  */
void setParameter() {
  
  Serial.println("ok-SET");
  
}

/* ======================   GET  =================== */

/*
 * 
 *
 *
  */
void getParameter(){
  
  Serial.print("ok-GET|");
  getEeprom();


}

/* ======================   GOTO  =================== */

/*
 * 
 *
 *
  */
void goToPosition() {
  
  Serial.println("ok-GOTO");
  
}

/* =====================================  SENSORS  ================================== */
/*
 * 
 * 
 * 
 */


/* ========================   READ TEMP  =================== */

/*
 * 
 */
void ReadTemp(){
  
  Serial.println("ok-LST|23");

}

/* ======================   READ HUMIDITY  =============== */

/*
 * 
 */
void ReadHumi(){
  
  Serial.println("ok-LHU|50");

}

/* ======================   READ TEMP FOC PRI  =============== */

/*
 * 
 */
void ReadFocPri(){
  
  Serial.println("ok-LFP|24");

}

/* ======================   READ TEMP FOC SEC  =============== */

/*
 * 
 */
void ReadFocSec(){
  
  Serial.println("ok-LFS|25");

}

/* ======================   READ ALL SENSOR   ================= */

/*
 * 
 */
void ReadAllSensors(){
  
  Serial.println("ok-LTE|26|25|25|26");

}



/* =========================   SET FAN   ====================== */

/*
 * 
 */
void SetVentole(){

    Serial.println("ok-CFN");  
    
}

/* ========================  SET RESISTOR  ==================== */

/*
 * 
 */
void SetScaldini(){
  
    Serial.println("ok-CPO"); 
    
}

/* ========================  GET OUTPUT  ====================== */

/*
 * 
 */
void GetOutput(){
  
    Serial.println("ok-COU| 150");   

}
/* ============================  SWITCH  ========================= */


void SetSwitch(){
  
    Serial.println("ok-SWC"); 
    
}


void StatusSingleSwitch(){
  
    Serial.println("ok-SWS|1"); 
    
}


void StatusSwitch(){
 
    Serial.println("ok-SWA|0 1|1 0|2 0|3 0|4 1|5 0|6 0|7 0"); 
    
}


/* ============================  LCD  ========================= */

/*
 * 
 */
void PrintLcd(){
  
    Serial.println("ok-CDS");

}

/* =======================  OTHER COMANDS  ===================== */

void debug(){

  Serial.println("ok-DEBUG");
  
}

void test(){

  Serial.println("ok-TEST");
  
}

void readAllSyncro(){

  if( skipAll ){
    return;
  }
  
  Serial.print("ALL|"); 
  
  //Serial.print(motori.status_Foc_1);
  Serial.print("|0");
  //Serial.print(motori.status_Foc_2);
  Serial.print("|1");
  //Serial.print(motori.status_Rot);
  Serial.print("|2");
  //Serial.print(motori.steps_Foc_1);
  Serial.print("|10000");
  //Serial.print(motori.steps_Foc_2);
  Serial.print("|20000");
  //Serial.print(motori.steps_Rot);
  Serial.print("|30000");

  
  //Serial.print(sensori.temp1);
  Serial.print("|24.1");
  //Serial.print(sensori.temp2);
  Serial.print("|24.2");
  //Serial.print(sensori.temp3);
  Serial.print("|24.3");
  //Serial.print(sensori.temp4);
  Serial.print("|24.4");  
  //Serial.print(sensori.hum);
  Serial.print("|67.22");

  
  //Serial.print(inputOutput.switchFocPriUp);
  Serial.print("|1");
  //Serial.print(inputOutput.switchFocPriDown);
  Serial.print("|0");
  //Serial.print(inputOutput.switchFocSecUp);
  Serial.print("|1");
  //Serial.print(inputOutput.switchFocSecDown);
  Serial.print("|0"); 
  //Serial.print(inputOutput.switchRotHome);
  Serial.print("|1");


  //Serial.print(motori.status_Pet);
  Serial.print("|3");
  //Serial.print(inputOutput.switchADx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchASx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchBDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchBSx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchCDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchCSx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchDDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchDSx);
  Serial.print("|0");    
  //Serial.print(inputOutput.switchOpen);
  Serial.print("|0");    
  //Serial.print(inputOutput.switchClose);
  Serial.print("|0");    
  //Serial.println(inputOutput.switchLaser);
  Serial.print("|1"); 

    // FAN
  Serial.print("|");
  Serial.print("200");
  Serial.print("|");
  Serial.print("112233");
  Serial.print("|");
  Serial.print("332211");
  Serial.print("|");
  Serial.print("221122");
  Serial.print("|");
  Serial.print("1");

  //RES
  Serial.print("|");
  Serial.print("125");
  Serial.print("|");
  Serial.print("2");

  //LASER
  Serial.print("|");
  int laserFinto = 1;
  Serial.println(laserFinto);
  
}

void readAll(){
  
  Serial.print("ok-ALL|"); 
  
  //Serial.print(motori.status_Foc_1);
  Serial.print("|0");
  //Serial.print(motori.status_Foc_2);
  Serial.print("|1");
  //Serial.print(motori.status_Rot);
  Serial.print("|2");
  //Serial.print(motori.steps_Foc_1);
  Serial.print("|10000");
  //Serial.print(motori.steps_Foc_2);
  Serial.print("|20000");
  //Serial.print(motori.steps_Rot);
  Serial.print("|30000");

  
  //Serial.print(sensori.temp1);
  Serial.print("|24.1");
  //Serial.print(sensori.temp2);
  Serial.print("|24.2");
  //Serial.print(sensori.temp3);
  Serial.print("|24.3");
  //Serial.print(sensori.temp4);
  Serial.print("|24.4");  
  //Serial.print(sensori.hum);
  Serial.print("|67.22");

  
  //Serial.print(inputOutput.switchFocPriUp);
  Serial.print("|1");
  //Serial.print(inputOutput.switchFocPriDown);
  Serial.print("|0");
  //Serial.print(inputOutput.switchFocSecUp);
  Serial.print("|1");
  //Serial.print(inputOutput.switchFocSecDown);
  Serial.print("|0"); 
  //Serial.print(inputOutput.switchRotHome);
  Serial.print("|1");


  //Serial.print(motori.status_Pet);
  Serial.print("|3");
  //Serial.print(inputOutput.switchADx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchASx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchBDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchBSx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchCDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchCSx);
  Serial.print("|0");
  //Serial.print(inputOutput.switchDDx);
  Serial.print("|1");
  //Serial.print(inputOutput.switchDSx);
  Serial.print("|0");    
  //Serial.print(inputOutput.switchOpen);
  Serial.print("|0");    
  //Serial.print(inputOutput.switchClose);
  Serial.print("|0");    
  //Serial.println(inputOutput.switchLaser);
  Serial.print("|1"); 

    // FAN
  Serial.print("|");
  Serial.print("200");
  Serial.print("|");
  Serial.print("112233");
  Serial.print("|");
  Serial.print("332211");
  Serial.print("|");
  Serial.print("221122");
  Serial.print("|");
  Serial.print("1");

  //RES
  Serial.print("|");
  Serial.print("125");
  Serial.print("|");
  Serial.print("2");

  //LASER
  Serial.print("|");
  int laserFinto = 1;
  Serial.println(laserFinto);
  
}

void manageAll(){
  
    char* flagAll = SCmd.next(); 
    
    if( *flagAll == '1' ){
      Serial.println("ok-ALU"); 
      skipAll = true;
    }
    else if( *flagAll == '0' ){
      Serial.println("ok-ALU"); 
      skipAll = false;
    }else{
      Serial.println("KO|001|parametro non riconosciuto"); 
    }
    
}


/*
 * DEFAULT COMAND RESPONSE
 */
void unrecognized()
{
  Serial.println("error"); 
}


/* ====================================================== */
/* ============= END CAMANDS CALLBACKS  ================= */
/* ====================================================== */

void getEeprom()
{  
        Serial.print(40);
        Serial.print("|"); 
        Serial.print(300);
        Serial.print("|");
        Serial.print(222);
        Serial.print("|");
        Serial.print(5);
        Serial.print("|");
        Serial.println(6);
}
